<?php

// tests/AppBundle/Service/CurrencyConverter.php
namespace Tests\AppBundle\Service;

use AppBundle\Service\CurrencyConverter;
use PHPUnit\Framework\TestCase;

class CurrencyConverterTest extends TestCase
{
    public function testConvertOrdersCurrencies()
    {
        $testOrdersXMLPath          = dirname(__FILE__).'/Orders.xml';
        $testExchangeRatesXMLPath   = dirname(__FILE__).'/ExchangeRates.xml';
        $testConvertedOrdersXMLPath = dirname(__FILE__).'/ConvertedOrders.xml';

        $currencyConverter      = new CurrencyConverter();
        $convertedCurrencyXML   = $currencyConverter->convertOrdersCurrencies($testOrdersXMLPath, $testExchangeRatesXMLPath);

        $this->assertXMLStringEqualsXmlFile($testConvertedOrdersXMLPath, $convertedCurrencyXML);
    }

}