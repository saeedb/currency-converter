<?php
/**
 * Created by PhpStorm.
 * User: saeed.bhuta
 * Date: 09/07/2018
 * Time: 14:52
 */

namespace Tests\AppBundle\Service;

use AppBundle\Service\ExchangeRate;
use AppBundle\Service\Order;
use AppBundle\Service\XMLProcessor;
use PHPUnit\Framework\TestCase;

class XMLProcessorTest extends TestCase
{
    public function testProcessOrdersXML()
    {
        $testOrdersXMLPath = dirname(__FILE__).'/Orders.xml';

        $xmlProcessor   = new XMLProcessor();
        $ordersData     = $xmlProcessor->processXML($testOrdersXMLPath, 'orders');

        $testOrderOne = new Order();
        $testOrderOne->setId('1');
        $testOrderOne->setDate('01/01/2016');
        $testOrderOne->setCurrencyCode('GBP');
        $testOrderOne->setAmount('9.98');

        $testOrderTwo = new Order();
        $testOrderTwo->setId('2');
        $testOrderTwo->setDate('02/01/2016');
        $testOrderTwo->setCurrencyCode('EUR');
        $testOrderTwo->setAmount('119.98');

        $expectedOrdersData = [
            $testOrderOne,
            $testOrderTwo
        ];

        $this->assertEquals($expectedOrdersData, $ordersData);
    }

    public function testProcessExchangeRatesXML()
    {
        $testExchangeRatesXMLPath   = dirname(__FILE__).'/ExchangeRates.xml';

        $xmlProcessor       = new XMLProcessor();
        $exchangeRatesData  = $xmlProcessor->processXML($testExchangeRatesXMLPath, 'exchangeRates');

        $testExchangeRateOne = new ExchangeRate();
        $testExchangeRateOne->setDate('01/01/2016');
        $testExchangeRateOne->setCurrencyName('Pound');
        $testExchangeRateOne->setCurrencyCode('GBP');
        $testExchangeRateOne->setCurrencyValue('1');
        $testExchangeRateOne->setForeignCurrencyValue('1.10');
        $testExchangeRateOne->setForeignCurrencyCode('EUR');

        $testExchangeRateTwo = new ExchangeRate();
        $testExchangeRateTwo->setDate('02/01/2016');
        $testExchangeRateTwo->setCurrencyName('Pound');
        $testExchangeRateTwo->setCurrencyCode('GBP');
        $testExchangeRateTwo->setCurrencyValue('1');
        $testExchangeRateTwo->setForeignCurrencyValue('1.09');
        $testExchangeRateTwo->setForeignCurrencyCode('EUR');

        $testExchangeRateThree = new ExchangeRate();
        $testExchangeRateThree->setDate('01/01/2016');
        $testExchangeRateThree->setCurrencyName('Euro');
        $testExchangeRateThree->setCurrencyCode('EUR');
        $testExchangeRateThree->setCurrencyValue('1');
        $testExchangeRateThree->setForeignCurrencyValue('0.89');
        $testExchangeRateThree->setForeignCurrencyCode('GBP');

        $testExchangeRateFour = new ExchangeRate();
        $testExchangeRateFour->setDate('02/01/2016');
        $testExchangeRateFour->setCurrencyName('Euro');
        $testExchangeRateFour->setCurrencyCode('EUR');
        $testExchangeRateFour->setCurrencyValue('1');
        $testExchangeRateFour->setForeignCurrencyValue('0.8');
        $testExchangeRateFour->setForeignCurrencyCode('GBP');

        $expectedExchangeRatesData = [
            $testExchangeRateOne,
            $testExchangeRateTwo,
            $testExchangeRateThree,
            $testExchangeRateFour
        ];

        $this->assertEquals($expectedExchangeRatesData, $exchangeRatesData);
    }
}