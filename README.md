Currency Converter 
=

This is a command line tool which takes two XML files as parameters:

1.  Orders - an xml file containing orders data.
2.  ExchangeRate - an xml file containing information currency rates on particular dates.

Using data from the provided Exchange Rates XML file, this tool will convert the currency of the order totals for each order in the provided Orders XML file. Data will be returned in XML format.

## Installing
Run the following command to install project dependencies.
```
composer install
```

## How to run
The command requires two parameters, orders xml and exchange rates xml:
```
php bin/console feelunique:convert-currency path/to/Orders.xml path/to/ExchangeRates.xml 
```

## Tests
PHPUnit tests can be run by the following command:
```
./vendor/bin/simple-phpunit
```

## Built with
* [Symfony](https://symfony.com/doc/3.4//index.html) The web framework used.



## Improvements

* Move Order and ExchangeRate classes to entity classes (if working with DB).
* In ExchangeRate::parseExchangeRates() replace nested loops by using DOMCrawler(?)/Xpath expressions (?).
* Convert individual items in order instead of just the order total.
* Rework to handle converting to more than one type of currency.
* Error handling, show message when file not found.
* Replace dates with DateTime format instead of string.

## Author
Saeed Bhuta