<?php
/**
 * Created by PhpStorm.
 * User: saeed.bhuta
 * Date: 09/07/2018
 * Time: 14:24
 */

namespace AppBundle\Service;

class XMLProcessor
{
    private function isFileSuitable($filePath)
    {
        if(is_readable($filePath)){
            return true;
        }else{
            return false;
        }
    }

    private function readXMLFileContents($xmlFile)
    {
        $xmlFileContents    = file_get_contents($xmlFile);
        $xmlData            = new \SimpleXMLElement($xmlFileContents);

        return $xmlData;
    }

    /**
     * Takes a given XML file of orders OR exchange rates data
     * and returns an array of Orders or ExchangeRates objects
     *
     * @param string $xmlPath - the path of the XML file
     * @param string $dataCategory - what data the XML file contains (string of 'orders' || 'exchangeRates')
     *
     * @return array|string
     */
    public function processXML($xmlPath, $dataCategory)
    {
        $data = [];
        if($this->isFileSuitable($xmlPath)){ // check if ordersXML file exists
            $xmlData = $this->readXMLFileContents($xmlPath);

            if($dataCategory == 'orders'){
                $data = $this->parseOrders($xmlData);
            }elseif ($dataCategory == 'exchangeRates'){
                $data = $this->parseExchangeRates($xmlData);
            }

        }else{
            return ('Problem accessing file');
        }

        return $data;
    }

    private function parseOrders($ordersXML)
    {
        $orders = [];
        foreach($ordersXML->order as $xmlOrder){
            $order = new Order();
            $order->setId((int)$xmlOrder->id);
            $order->setCurrencyCode((string)$xmlOrder->currency);
            $order->setDate((string)$xmlOrder->date);

            $order->setAmount((string)$xmlOrder->total);

            $orders[] = $order;
        }

        return $orders;
    }

    private function parseExchangeRates($exchangeRatesXML)
    {
        $exchangeRates = [];
        foreach($exchangeRatesXML->currency as $currency){
            $currencyName = (string)$currency->name;
            $currencyCode = (string)$currency->code;

            foreach ($currency->rateHistory as $rates){
                foreach($rates as $rate){
                    $exchangeRate = new ExchangeRate();
                    $exchangeRate->setCurrencyName($currencyName);
                    $exchangeRate->setCurrencyCode($currencyCode);

                    $exchangeRate->setDate((string)$rate['date']);

                    foreach($rate->rate as $rateDetails){
                        if($rateDetails['code'] != $currencyCode){
                            $exchangeRate->setForeignCurrencyCode((string)$rateDetails['code']);
                            $exchangeRate->setForeignCurrencyValue((string)$rateDetails['value']);
                        }elseif($rateDetails['code'] == $currencyCode){
                            $exchangeRate->setCurrencyValue($rateDetails['value']);
                        }
                    }
                    $exchangeRates[] = $exchangeRate;
                }
            }
        }

        return $exchangeRates;
    }
}