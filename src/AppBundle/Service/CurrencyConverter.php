<?php
/**
 * Created by PhpStorm.
 * User: saeed.bhuta
 * Date: 06/07/2018
 * Time: 14:50
 */

namespace AppBundle\Service;

class CurrencyConverter
{

    /**
     * Returns the exchange rate for a given currency on a given date
     *
     * @param array $exchangeRates - an array of ExchangeRate objects
     * @param string $currencyCode - currency code of the currency we are converting from
     * @param string $date - date of order
     *
     * @return array $exchangeRate - array containing exchange rate and currency code
     */
    private function getExchangeRate($exchangeRates, $currencyCode, $date)
    {
        // search through exchangeRates for our given currencyCode and date
        $exchangeRateData = ['currencyCode' => null, 'exchangeRate' => null];
        foreach($exchangeRates as $rate){
            if(($rate->getCurrencyCode() == $currencyCode) && ($rate->getDate() == $date)){
                $exchangeRate = $rate->getForeignCurrencyValue();
                $currencyCode = $rate->getForeignCurrencyCode();

                $exchangeRateData['currencyCode'] = $currencyCode;
                $exchangeRateData['exchangeRate'] = $exchangeRate;
                break;
            }
        }
        return $exchangeRateData;
    }

    private function convertCurrency($amount, $exchangeRate)
    {
        return bcmul($amount, $exchangeRate, 2);
    }

    /**
     * Takes a list of orders (formatted in XML) and exchange rates data (formatted in XML).
     * Using the data in the exchange rates XML, the order totals are converted to another currency.
     * The result is output in XML.
     *
     * @param $ordersXMLFilePath
     * @param $exchangeRatesXMLFilePath
     *
     * @return string $convertedOrdersXML
     */
    public function convertOrdersCurrencies($ordersXMLFilePath, $exchangeRatesXMLFilePath)
    {
        // parse orders + exchange rate from xml files (read into array of objects)
        $xmlProcessor   = new XMLProcessor();
        $orders         = $xmlProcessor->processXML($ordersXMLFilePath, 'orders');
        $exchangeRates  = $xmlProcessor->processXML($exchangeRatesXMLFilePath, 'exchangeRates');

    	// loop through orders
        $ordersData = [];
        foreach($orders as $order){
            $orderDate      = $order->getDate();
            $currencyCode   = $order->getCurrencyCode();
            $orderAmount    = $order->getAmount();

            $exchangeRateData       = $this->getExchangeRate($exchangeRates, $currencyCode, $orderDate);
            $exchangeRate           = $exchangeRateData['exchangeRate'];
            $convertCurrencyCode    = $exchangeRateData['currencyCode'];

            // convert order total
            $convertedAmount = $this->convertCurrency($orderAmount, $exchangeRate);

            // format order details and converted amount into array for outputting in xml
            $ordersData[] = array(
                'id'    => $order->getId(),
                'date'  => $orderDate,
                'currencies' => array(
                    array(
                        'currencyCode'  => $currencyCode,
                        'total'         => $orderAmount
                    ),
                    array(
                        'currencyCode'  => $convertCurrencyCode,
                        'total'         => $convertedAmount
                    )
                )
            );
        }
        return $this->createXML($ordersData);
    }

    /**
     * Converts an array of orders into xml
     *
     * @param $convertedOrders
     *
     * @return string $xml
     */
    private function createXML($convertedOrders)
    {
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><orders></orders>');

        foreach ($convertedOrders as $orderData){
            $orderXMLElem = $xml->addChild('order');

            $orderXMLElem->addChild('id', $orderData['id']);
            $orderXMLElem->addChild('date', $orderData['date']);

            $currenciesXMLElm = $orderXMLElem->addChild('currencies');

            foreach ($orderData['currencies'] as $currencyData){
                $currencyXMLElem = $currenciesXMLElm->addChild('currency');
                $currencyXMLElem->addChild('currencyCode', $currencyData['currencyCode']);
                $currencyXMLElem->addChild('total', $currencyData['total']);
            }
        }

        return $xml->asXML();
    }

}