<?php
/**
 * Created by PhpStorm.
 * User: saeed.bhuta
 * Date: 09/07/2018
 * Time: 11:06
 */

namespace AppBundle\Service;

class ExchangeRate
{
    private $date;
    private $currencyName;
    private $currencyCode;
    private $currencyValue;
    private $foreignCurrencyCode;
    private $foreignCurrencyValue;

    /**
     * @return mixed
     */
    public function getForeignCurrencyValue()
    {
        return $this->foreignCurrencyValue;
    }

    /**
     * @param mixed $foreignCurrencyValue
     */
    public function setForeignCurrencyValue($foreignCurrencyValue)
    {
        $this->foreignCurrencyValue = $foreignCurrencyValue;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCurrencyName()
    {
        return $this->currencyName;
    }

    /**
     * @param mixed $currencyName
     */
    public function setCurrencyName($currencyName)
    {
        $this->currencyName = $currencyName;
    }

    /**
     * @return mixed
     */
    public function getCurrencyCode()
    {
        return $this->currencyCode;
    }

    /**
     * @param mixed $currencyCode
     */
    public function setCurrencyCode($currencyCode)
    {
        $this->currencyCode = $currencyCode;
    }

    /**
     * @return mixed
     */
    public function getForeignCurrencyCode()
    {
        return $this->foreignCurrencyCode;
    }

    /**
     * @param mixed $foreignCurrencyCode
     */
    public function setForeignCurrencyCode($foreignCurrencyCode)
    {
        $this->foreignCurrencyCode = $foreignCurrencyCode;
    }

    /**
     * @return mixed
     */
    public function getCurrencyValue()
    {
        return $this->currencyValue;
    }

    /**
     * @param mixed $currencyValue
     */
    public function setCurrencyValue($currencyValue)
    {
        $this->currencyValue = $currencyValue;
    }

}