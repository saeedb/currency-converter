<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Service\CurrencyConverter;

class FeeluniqueConvertCurrencyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('feelunique:convert-currency')
            ->setDescription('A script which takes two XML files (orders and currencies) and converts the order currencies')
            ->addArgument('ordersXMLFilePath', InputArgument::REQUIRED, 'An XML file containing order details')
            ->addArgument('exchangeRatesFilePath', InputArgument::REQUIRED, 'An XML file containing exchange rate details')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ordersXMLFilePath          = $input->getArgument('ordersXMLFilePath');
        $exchangeRatesXMLFilePath   = $input->getArgument('exchangeRatesFilePath');

        $currencyConverter      = new CurrencyConverter();
        $convertedOrdersXML     = $currencyConverter->convertOrdersCurrencies($ordersXMLFilePath, $exchangeRatesXMLFilePath);

        $output->writeln($convertedOrdersXML);
    }

}
